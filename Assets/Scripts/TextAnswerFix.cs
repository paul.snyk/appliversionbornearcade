﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TextAnswerFix : MonoBehaviour
{
    public GameObject textObject;
    public GameObject cardFollow;
    public GameObject centerBg;

    private Vector3 iniPos;
    private Vector3 endPos;

    private void Start()
    {
        iniPos = new Vector3(textObject.transform.position.x, 0, 0);
    }

    void Update()
    {
        endPos = new Vector3(cardFollow.transform.position.x, 0, 0);

        textObject.transform.position = new Vector3(Mathf.Clamp(0.5f, textObject.transform.position.x, endPos.x), 0, textObject.transform.position.z);
    }
}
