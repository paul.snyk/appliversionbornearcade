﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Card : MonoBehaviour
{
    public string dialogue;
    public string rightQuote;
    public string leftQuote;

    public Material materialPanel;

    bool answer;

    //Choix de droite
    public void Right()
    {
        Scene sceneManager = SceneManager.GetActiveScene();

        if (sceneManager.buildIndex == 1)
        {
            DataBase dataBase = GameObject.FindObjectOfType<DataBase>();
            ScoreBar scoreBar = GameObject.FindObjectOfType<ScoreBar>();

            //Si le choix est bon
            if (dataBase.index[6].Contains("R/"))
            {
                //increment score
                scoreBar.IncrementScore(float.Parse(dataBase.index[2]));
                materialPanel.color = Color.green;

                answer = true;
                GameObject.FindObjectOfType<FactDisplay>().soundCheck = true;
                GameObject.FindObjectOfType<FactDisplay>().Sound(answer);
            }
            //Si le choix est mauvais
            else
            {
                //increment score
                scoreBar.IncrementScore(float.Parse(dataBase.index[3]));
                materialPanel.color = Color.red;

                answer = false;
                GameObject.FindObjectOfType<FactDisplay>().soundCheck = true;
                GameObject.FindObjectOfType<FactDisplay>().Sound(answer);

                GameObject.FindObjectOfType<FactDisplay>().falseAnswer = true;
            }
        }
    }

    //Choix de gauche
    public void Left()
    {
        Scene sceneManager = SceneManager.GetActiveScene();

        if (sceneManager.buildIndex == 1)
        {
            DataBase dataBase = GameObject.FindObjectOfType<DataBase>();
            ScoreBar scoreBar = GameObject.FindObjectOfType<ScoreBar>();

            //Si le choix est bon
            if (dataBase.index[5].Contains("R/"))
            {
                scoreBar.IncrementScore(float.Parse(dataBase.index[2]));
                materialPanel.color = Color.green;

                answer = true;
                GameObject.FindObjectOfType<FactDisplay>().soundCheck = true;
                GameObject.FindObjectOfType<FactDisplay>().Sound(answer);
            }
            //Si le choix est mauvais
            else
            {
                scoreBar.IncrementScore(float.Parse(dataBase.index[3]));
                materialPanel.color = Color.red;

                answer = false;
                GameObject.FindObjectOfType<FactDisplay>().soundCheck = true;
                GameObject.FindObjectOfType<FactDisplay>().Sound(answer);

                GameObject.FindObjectOfType<FactDisplay>().falseAnswer = true;
            }
        }
    }
}

