﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class moveCardJoystick : MonoBehaviour
{
    private Transform _transform;

    private float speedPlayer = 9f;
    public float positionX;
    public Vector2 initialposition;

    public bool positionjoystick;
    // Start is called before the first frame update
    void Start()
    {
        _transform = GetComponent<Transform>();
        initialposition = new Vector2(_transform.position.x,_transform.position.y);
        positionjoystick = true;
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetAxis("Horizontal") == 0)
        {
            positionjoystick = false;
        }
        if (!positionjoystick)
        {
           if (Input.GetButtonUp("Horizontal"))
           {
               _transform.position = initialposition;
            }
            positionX = Input.GetAxis("Horizontal");
            Vector3 positionPlayer = new Vector3(positionX, 0, 0) * Time.deltaTime * speedPlayer;
            _transform.Translate(positionPlayer);
            _transform.position = new Vector3(Mathf.Clamp(_transform.position.x,-6f,6f),_transform.position.y,_transform.position.z); 
        }
    }
}
