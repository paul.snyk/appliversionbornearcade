﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using DG.Tweening;

public class ScoreBar : MonoBehaviour
{
    
    public Image barScore;
    static float score = 0.5f;
    public Image damageBar;
    public Image healBar;
    public Color damageBarColor;
    public Color healBarColor;
    public float timeColor;


    public void Start()
    {
        barScore.fillAmount = score;//au lancement le score est pile au middle de la barre
        damageBarColor = damageBar.color;
        healBarColor = healBar.color;
        damageBarColor.a = 0f;
        healBarColor.a = 0f;
        damageBar.color = damageBarColor;
        healBar.color = healBarColor;
    }
    public void IncrementScore(float amount)
    {
        if (amount < 0)
        {
            damageBar.fillAmount = barScore.fillAmount;
            damageBarColor.a = 1f;
            barScore.fillAmount += amount / 100;
            
            Debug.Log("c'est la diminution");
        }
        else if (amount > 0)
        {
            
            healBar.fillAmount = barScore.fillAmount + 0.05f;
            healBarColor.a = 1f;
            healBar.color = healBarColor;
            Debug.Log(healBarColor.a + "alpha");
            Debug.Log(healBar.fillAmount + "fillamount");
            
            //barScore.fillAmount += amount / 100;
            Debug.Log("c'est l'augmentation");
        }
        
    }

    public void Update()
    {
        if (damageBarColor.a > 0)//boucle va déscendre tranquillement la bar rouge sur le score
        {
            timeColor -= Time.deltaTime;
            if (timeColor < 0)
            {
                float fadeTime = 2f;
                damageBarColor.a -= fadeTime * Time.deltaTime;
                damageBar.color = damageBarColor;
            }
        }

        if (healBarColor.a > 0)
        {
            Debug.Log("activation if 0");
            timeColor -= Time.deltaTime;
            if (timeColor < 0)
            {
                Debug.Log("if time color");
                float fadeTimeHeal = 2f;
                healBarColor.a -= fadeTimeHeal * Time.deltaTime;
                if (healBarColor.a <= 0)
                {
                    Debug.Log("barscore augmentation");
                    barScore.fillAmount += 0.05f;
                    healBar.color = healBarColor;
                }
        
        
            }
        }
        
        if (barScore.fillAmount <= 0)
        {
            //Loose
            SceneManager.LoadScene(3);
        }
        else if (barScore.fillAmount >= 1 && !GameObject.FindObjectOfType<FactDisplay>().check)
        {
            //Win
            SceneManager.LoadScene(2);
        }

        if (GameObject.FindObjectOfType<DataBase>().listData.Count <= 1)
        {
            //Nouvelle pioche
            score = barScore.fillAmount;
            SceneManager.LoadScene(1);
        }
    }
}
