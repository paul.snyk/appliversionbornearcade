﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ButtonQuit : MonoBehaviour
{
    public GameObject panelQuit;
    public GameManager gameManager;
    public bool gameIsPaused;

    //Active l'écran de pause
    public void Update()
    {
        if (Input.GetButtonDown("Escape"))
        {
            if (gameIsPaused)
            {
                panelQuit.SetActive(false);
                ReturnGame(); 
            }
            else if (!gameIsPaused)
            {
                panelQuit.SetActive(true);
                PausePanel(); 
            }
        }
        if (gameIsPaused)
        {
            gameManager.dragging = false;
            if (Input.GetButtonDown("Validation"))
            {
                ReturnMainMenu();
            }
        }
    }

    public void PausePanel()
    {
        Time.timeScale = 0;
        gameIsPaused = true;
    }

    //Retour au jeu
    public void ReturnGame()
    {
        Time.timeScale = 1;
        gameIsPaused = false;
    }

    //Retourner à l'écran principal
    public void ReturnMainMenu()
    {
        Time.timeScale = 1;
        SceneManager.LoadScene(0);
    }
}
